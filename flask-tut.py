from flask import Flask,request, render_template
import subprocess

# function to execute a linux command
# the command is presented in the form of a list
#def executeCommand(myCommand):
def executeCommand():
	myCommand=["ping", "-c 25","cnn.com"]
	res=subprocess.check_output(myCommand)
	myLs=res.decode('ascii')
#	print(myLs)
	return myLs

app=Flask(__name__)
@app.route('/')
def index():
	return '<h2>Method used: %s' % request.method

@app.route('/profile/<name>')
def profile(name):
	return render_template("profile.html",name=name)

@app.route('/social')
@app.route('/social/')
@app.route('/social/<user>')
def social(user=None):
	return render_template("user.html",user=user)
@app.route("/shopping")
def shopping():
	food=["cheese","Tuna","Beef"]
	return render_template("shopping.html",food=food)
@app.route('/post/<int:post_id>')
def post(post_id):
	return "<h2>Post ID is %s<h2>" % post_id

@app.route('/ping')
def ping():
	#myresults=executeCommand("uname -a")
	myresults=executeCommand()
	return myresults

if __name__ == "__main__":
	app.run(debug=True)